<?php 
if ($_GET['page']='msg-lab'){
?>
            <div class="row">
                <div class="col-lg-12">
					<h3 class="page-header"><strong>Data Pengaduan</strong></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <a class="btn btn-info btn-sm" href="export.php?act=exp-msg-lab">PRINT</a>
                        
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                        <th class="text-center">NO</th>
                                            <th class="text-center">NIM</th>
                                           <th class="text-center">NAMA</th>
                                            <th class="text-center">PESAN</th>
                                            <th class="text-center">KONFIRMASI</th>
                                            <th class="text-center">TANGGAL</th>
                                           <th class="text-center">AKSI</th>
                                        </tr>
                                    </thead>
                                    <tbody>
<?php
    $no=1;
	$sql=mysqli_query($conn,"SELECT p.nim,p.nama,bau.* FROM pengguna as p,pengaduan_lab as bau WHERE p.nim=bau.nim ORDER BY nama ASC");
    
	while($rs=mysqli_fetch_assoc($sql)){

?>                            

                            <tr class="odd gradeX">
                                            <td class="text-center"><?php echo $no; ?></td>
                                            <td ><?php echo"$rs[nim]";  ?></td>
                                            <td ><?php echo strtoupper($rs['nama']);  ?></td>
                                            <td ><?php echo strtoupper($rs['pesan']);   ?></td>
                                            <td ><?php echo strtoupper($rs['konfirmasi']);   ?></td>
                                            <td ><?php echo"$rs[tanggal]";  ?></td>
                                             <td class="text-center"><a href="index.php?page=msg-lab-reply&act=send-msg-lab&id=<?php echo $rs['id_pengaduan'] ?>"><button type="button" class="btn btn-warning btn-xs" >Balas</button> 
                                        </tr>
                                        
<?php
$no++;
    
    }

?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
<?php
}
?>