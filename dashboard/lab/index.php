<?php

session_start();
if(!empty($_SESSION['username'])){
$username=$_SESSION['username'];
$idu=$_SESSION['id_user'];
$nama=$_SESSION['nama'];
$level=$_SESSION['level'];

include "../../config/config.php";
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Aplikasi Quick Response Pengaduan Kerusakan</title>

    <!-- Core CSS - Include with every page -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Tables -->
    <link href="../../assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="../../assets/css/sb-admin.css" rel="stylesheet">
    <script src="../../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/cek.js"></script>
    <script type="text/javascript" src="../../assets/js/datepicker.js"></script>
</head>

<body onload='process()'>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php?page=home">Quick Response pengaduan Kerusakan</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
           
                </li>
 <li>
                    <a>
<?php echo $nama; ?> 
                   </a>
                </li>
<li>
 
                    <a class="dropdown-toggle" href="../../logout.php">
<?php echo "Logout"; ?> 
                   </a>
                </li>                
                <li class="dropdown">
                    <a>
<?php echo "Tanggal : ".date("d-m-Y"); ?> 
                   </a>
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>

						
    
  
                        </li>             
                          <li><a href="index.php?page=msg-lab">PESAN PENGADUAN<span class="fa arrow"></span></a>
                            
                        </li>
                        <li><a href="#">DATA USER<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                      
                                <li><a href="index.php?page=input-user">Input User</a></li>
                                <li><a href="index.php?page=view-user">View User</a></li>
                            </ul>
                        </li>
                       

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
<?php include "content.php";  ?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="../../assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="../../assets/js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

</body>

</html>

<?php }?>